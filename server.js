var express = require('express');
var app = express();
var port = 3000 ;
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var p = require('./pool.js');
var timeoutid;

app.use(express.static('www/'));

p.reset();

app.get("/",function(req,res){
  res.sendFile('www/app.html', {root: __dirname});
});

http.listen(port, function(){
  console.log('listening on : '+port);
});

io.on('connection', function(socket){
  socket.emit('update players', p.get_clients());

  socket.on('connected', function(name){
    console.log(name+' connected');
    socket.emit(p.connected(name,socket));
    io.emit('player connected',name);
    if(p.is_reading())
    {
      socket.emit('in-progress');
    }
    else
    {
      socket.emit('state change', p.get_state());
    }
  });

  socket.on('state change', function(s){
    p.new_state(s);
    io.emit('state change', s);
  });

  socket.on('new word', function(mot){
    p.add_word(mot);
    var a = p.get_admin()
    if(a != null) {
      a.emit('update words', p.get_words());
    }
  });

  socket.on('remove word', function(i){
    p.remove_word(i);
    socket.emit('update words', p.get_words());
  });

  socket.on('my turn', function(){
    socket.broadcast.emit('turn chosen');
    socket.emit('your turn');
    p.set_reading(true);
    p.reset_words_read();
    socket.emit('next word', p.next_word());
    io.emit('word count', p.active_words());
    io.emit('start timer', p.get_time());

    console.log("Sarting timer... (time=" + p.get_time() + ")");

    timeoutid = setTimeout(function (){
      console.log("Time is over");
      p.set_reading(false);
      p.skipped_back();
      io.emit('turn over', p.get_words_guessed());
      io.emit('word count', p.word_count());
    }, (parseInt(p.get_time()) + 1)*1000);
  });

  socket.on('skip word', function(){
    socket.emit('next word', p.skip_word());
    io.emit('word count', p.active_words());
  });

  socket.on('next word', function(){
    var m = p.next_word();
    if(m!=null)
    {
      socket.emit('next word', m);
      io.emit('word count', p.active_words());
    }
    else
    {
      clearTimeout(timeoutid);
      io.emit('end of round', p.get_words_guessed());
      p.new_state('pregame');
      p.set_reading(false);
    }
  });

  socket.on('start game', function(){
    console.log(p.total_words());
    if(p.total_words()>=1)
    {
      p.start_game();
      p.new_state('ingame');
      io.emit('state change', 'ingame');
      io.emit('word count', p.word_count());
    }
  });

  socket.on('reset game', function(){
    p.reset();
    console.log('RESET');
    socket.emit('reset confirmed');
  });

  socket.on('set time', function(time){
    p.change_time(time);
  });

  socket.on('disconnect', function(){
    var i = p.disconnected(socket);
    if(i!=null)
    {
      io.emit('player disconnected',i);
      if(i==0)
      {
        var admin = p.get_admin();
        admin.emit('admin');
        admin.emit('update words', p.get_words);
      }
    }
  });

});
