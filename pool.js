var all_words=[];
var words=[];
var words_guessed = [];
var words_skipped = [];
var time = 30;
var admin = false;
var clients=[];
var sockets=[];
var state = null;
var reading = false;
var current_word = null;


function random(max)
{
    return Math.floor(Math.random() * Math.floor(max));
}

function reset_game()
{
    all_words.splice(0);
    words.splice(0);
    words_guessed.splice(0);
    words_skipped.splice(0);
    state = null;
    reading = false;
    time = 30;
    current_word = null;
}

module.exports = {
add_word: function (mot)
{
    all_words.push(mot);
    console.log("New Word : " + mot);
},

new_state: function(s)
{
    state = s;
    console.log('state changed to ' + s);
},

start_game: function ()
{
    words = all_words.slice(0);
    words_guessed.splice(0);
    words_skipped.splice(0);
    console.log('Game initialised');
},

next_word: function ()
{
    if(current_word!=null)
    {
        words_guessed.push(words[current_word]);
        words.splice(current_word,1);
    }

    if(words.length<=0)
    {
        if(words_skipped.length>0)
        {
            this.skipped_back();
        }
        else
        {
            return null;
        }
    }

    current_word = random(words.length);
    console.log("Next word is " + words[current_word]);
    return words[current_word];
},

active_words: function ()
{
    return words.length + words_skipped.length - 1;
},

skip_word: function ()
{
    if(words.length<=1)
    {
        this.skipped_back();
    }

    if(words.length>1)
    {
        words_skipped.push(words[current_word]);
        words.splice(current_word,1);
        current_word = random(words.length);
    }

    return words[current_word];

},

reset : function ()
{
    reset_game();
},

word_count: function()
{
    return words.length;
},

remove_word: function(i)
{
    console.log('Removing ' + all_words[i]);
    all_words.splice(i,1);
},

total_words : function()
{
    return all_words.length;
},

get_admin : function ()
{
    return sockets[0];
},

get_state : function ()
{
    return state;
},

get_time : function ()
{
    return time;
},

get_clients : function()
{
    return clients;
},

get_sockets : function()
{
    return sockets;
},

get_words : function()
{
    return all_words;
},

get_words_guessed: function()
{
    return words_guessed;
},

skipped_back : function()
{
    for(i=0; i<words_skipped.length; i++)
    {
        words.push(words_skipped[i]);
    }
    words_skipped.splice(0);
    console.log('words skipped put back in word bank');
},

reset_words_read: function()
{
    words_guessed.splice(0);
    current_word = null;
    console.log('words read reset');
},

is_reading: function()
{
    return reading;
},

set_reading: function(s)
{
    reading=s;
    console.log('reading changed to ' + s);
},

connected : function (name,socket)
{
    sockets.push(socket);
    clients.push(name);
    if(admin==false)
    {
        admin=true;
        return 'admin';
    }
    else
    {
        return 'client';
    }
},

disconnected : function(socket)
{
    var i = sockets.indexOf(socket);
    if(i!=-1)
    {
        console.log(clients[i] + ' disconnected');
        sockets.splice(i,1);
        clients.splice(i,1);
        if(sockets.length==0)
        {
            reset_game();
            admin = false;
            return null;
        }
        return i;
    }
    else
        return null;
},

change_time : function(new_time)
{
  console.log("timer set to " + new_time);
    time = new_time;
}

};
