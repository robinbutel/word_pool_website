var its_my_turn = false;

function connect_to_the_world(name) {
  console.log("Connecting...");

  connect(name);
}

function admin_letsgo(timer) {
  set_timer(timer);
  start_word();
}

function words_starting() {
  if(is_admin()) {
    $("#start-game").show();
  } else {
    $("#start-game").hide();
  }
  go_to_div(".words");
}

function we_can_start_this_fun_game() {
  $('.turn').show();
  $('.my-turn').hide();
  $('.not-your-turn').hide();
  $('#time-over').hide();
  go_to_div(".game");
}

function not_your_turn() {
  its_my_turn = false;
  $(".turn").hide();
  $(".not-your-turn").show();
}

function your_turn() {
  its_my_turn = true;
  $(".turn").hide();
  $(".my-turn").show();
}

var timer = 0;
var intervalid;

function timer_func() {
  timer -= 1;
  if(timer <= 0) {
    $(".timer-nb").text("0");
    clearInterval(intervalid);
  }

  $(".timer-nb").text(timer);
}

function round_over(words_get) {
  clearInterval(intervalid);
  go_to_div(".round-over");
  $(".nb-words-end").text(words_get.length);
  $(".round-over .gwords-list").show();

  console.log("words guessed : " + words_get);
  $(".round-over .gwords-list .items").html("");
  words_get.forEach((e, i) => {
    $(".round-over .gwords-list .items").append("<p>" + e + "</p>");
  });

  if(is_admin()) {
    $("#admin-start-round").show();
    $('#admin-reset-game').show();
  } else {
    $("#admin-start-round").hide();
    $('#admin-reset-game').hide();
  }

  $(".turn .gwords-list").hide();
}

function lets_start_next_round() {
  start_game();
}

function refresh_words_left(wl) {
  console.log(wl + " words left");
  $(".words-left-nb").text(wl);
}

function start_timer(t) {
  console.log("timer starting... (time=" + t + ")");
  timer = t;
  intervalid = setInterval(timer_func, 1000);
}

function stop_timer(words_get) {
  $(".timer-nb").text("0");
  clearInterval(intervalid);

  $("#time-over").show();
  $(".turn .gwords-list").show();

  if(its_my_turn) {
    console.log("time over it was your turn");
    $(".my-turn").hide();
    $(".turn #turn-btn").hide();
    $(".turn").show();
    setTimeout(() => { $(".turn #turn-btn").show() }, 1000);
  } else {
    $(".not-your-turn").hide();
    $(".turn").show();
  }

  $(".nb-words-dis").show();
  $(".nb-words-dis .nb-words").text(words_get.length);

  console.log("words guessed : " + words_get);
  $(".turn .gwords-list .items").html("");
  words_get.forEach((e, i) => {
    $(".turn .gwords-list .items").append("<p>" + e + "</p>");
  });

  its_my_turn = false;
}

function send_word(event) {
  var word = $("#words").val();
  $("#words").val("");

  new_word(word);

  show_notif("Mot envoyé", 1000);
}

function show_notif(msg, time) {
  $("#info-notif").show(200);
  $("#info-notif .notif-text").text(msg);
  setTimeout(function() { $("#info-notif").hide(200); }, time);
}

function give_me_next_word() {
  next_word();
  $(".word-panel").text("...");
}

function just_skip_that_word_idk() {
  skip_word();
  $(".word-panel").text("...");
}

function show_next_word(w) {
  $(".word-panel").text(w);
}

function game_state(state) {
  $(".panel-state").html(state);
}

function you_are_connected(name, isadmin) {
  if(name !=  "") {
    $(".panel-userstate").html("Connecté en tant que <p>" + name + (isadmin ? " <span class=\"youareadmin\">(admin)</span></p>" : "</p>"));
    if(isadmin) {
      $(".admin-settings-btn").show();
      go_to_div(".admin-setup");
    } else {
      $(".admin-settings-btn").hide();
      go_to_div(".waiting");
    }
  } else {
    $(".panel-userstate").html("Déconnecté");
    go_to_div(".main-menu");
  }
}

function refresh_clients(clients) {
  if(clients.length == 0) {
    $(".panel-title").html("Aucun Utilisateurs connectés");
    $(".user-list").html("");
  } else {
    $(".panel-title").html("Utilisateurs connectés");
    $(".user-list").html("");
    clients.forEach((name, i) => {
      var html = "";
      if(i == 0) {
        html = "<div class=\"user-item\">" + name + " <span class=\"is-admin\">(admin)</span></div>";
      } else {
        html = "<div class=\"user-item\">" + name + "</div>";
      }
      $(".user-list").append(html);
    });
  }
}

function refresh_words() {
  var words = get_words();
  if(words.length == 0) {
    $(".admin-menu .word-list").html("Aucun mots pour l'instant");
    return;
  }

  $(".admin-menu .word-list").html("");
  var html = "";
  words.forEach((e, i) => {
    html = "<div id=\"" + i + "\" class=\"element\"><span class=\"word\">" + e + "</span><span class=\"delete-item\">" +
      '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-delete"><path d="M21 4H8l-7 8 7 8h13a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"></path><line x1="18" y1="9" x2="12" y2="15"></line><line x1="12" y1="9" x2="18" y2="15"></line></svg>'
      + "</span></div>";
    $(".admin-menu .word-list").append(html);
  });
  $(".admin-menu .word-list .delete-item").click((e) => { delete_that_word_i_dont_want(e.currentTarget); });
}

function delete_that_word_i_dont_want(selected) {
  var e = $(selected).closest(".element");
  var i = parseInt($(e).attr("id"));
  console.log("removing word: " + e + " (index=" + i + ")");
  $(e).remove();
  remove_word(i);
  setTimeout(refresh_words, 500);
}

function toggle_admin_menu() {
  var shown = $(".admin-menu").is(":visible");
  refresh_words();
  if(shown) {
    $(".admin-menu").hide(500);
    $(".admin-settings-btn").css({ right: "0px" });
  } else {
    $(".admin-menu").show(500);
    setTimeout(() => { $(".admin-settings-btn").css({ right: $(".admin-menu").width() + 22 + "px" }); }, 550);
  }
}

function username_press(e) {
  console.log("name : " + $("#username").val());
  name = $("#username").val();
  $("#username").val("");

  connect_to_the_world(name);
}

var current_div = ".main-menu";

function go_to_div(div) {
  if(current_div != div && $(div).length != 0) {
    console.log("going from " + current_div + " to " + div);
    $(div).show(300);
    $(current_div).hide(300);
    current_div = div;
  }
}

$(function() {
  $("#admin-letsgo").click((e) => {
    var timer = document.getElementById("rs-range-line").value;
    admin_letsgo(timer);
  });

  $(".main-menu .play-btn").click((e) => { go_to_div(".username"); });

  $('#username').on('keydown', function(e) {
    if (e.which == 13) {
      username_press(e);
      e.preventDefault();
    }
  });
  $("#username-btn").click(username_press);

  $('#words').on('keydown', function(e) {
    if (e.which == 13) {
      send_word(e);
      e.preventDefault();
    }
  });
  $("#words-btn").click(send_word);

  $("#turn-btn").click(my_turn);

  $("#next-btn").click(give_me_next_word);

  $("#skip-btn").click(just_skip_that_word_idk);

  $("#admin-start-round").click(lets_start_next_round);

  $("#admin-reset-game").click(reset_game);

  $(".admin-menu .word-list .delete-item").click((e) => { delete_that_word_i_dont_want(e.currentTarget); });
  $(".admin-settings-btn").click(toggle_admin_menu);
  $(".admin-menu #add-word-btn").click((e) => {
    var word = $(".admin-menu #newword").val();
    $(".admin-menu #newword").val("");

    show_notif("Mot ajouté", 1000);
    new_word(word);
    setTimeout(refresh_words, 500);
  });
  $(".admin-menu #change-time-btn").click((e) => {
    show_notif("Timer changé", 1000);
    var time = $(".admin-menu #admin-slider-line").val();
    set_timer(time);
  });

  $(".notif-close").click(function(e) {
    $(e.currentTarget).parents(".notif").hide(100);
  });

  $("#start-game").click(() => {
    start_game();
  });

  game_state("Aucune partie en cours");
  you_are_connected("", false);
  refresh_clients([]);

  setInterval(() => {
    refresh_clients(get_clients());
  }, 2000);

  /* Main Slider */
  var rangeSlider = document.getElementById("rs-range-line");
  var rangeBullet = document.getElementById("rs-bullet");
  rangeSlider.value = 30;

  showSliderValue();
  rangeSlider.addEventListener("input", showSliderValue, false);

  function showSliderValue() {
    rangeBullet.innerHTML = rangeSlider.value;
    var bulletPosition = ((rangeSlider.value-rangeSlider.min) / (rangeSlider.max - rangeSlider.min));
    rangeBullet.style.left = (bulletPosition * 350) + "px";
  }

  /* Admin menu Slider */
  var adminslider = document.getElementById("admin-slider-line");
  var adminbullet = document.getElementById("admin-slider-txt");
  adminslider.value = 30;

  adminshowSliderValue();
  adminslider.addEventListener("input", adminshowSliderValue, false);

  function adminshowSliderValue() {
    adminbullet.innerHTML = adminslider.value;
    var bulletPosition = ((adminslider.value-adminslider.min) / (adminslider.max - adminslider.min));
    adminbullet.style.left = (bulletPosition * 350) + "px";
  }

  /**
   * jQuery.browser.mobile (http://detectmobilebrowser.com/)
   *
   * jQuery.browser.mobile will be true if the browser is a mobile device
   *
   **/
  (function(a){(jQuery.browser=jQuery.browser||{}).mobile=/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))})(navigator.userAgent||navigator.vendor||window.opera);

  if(jQuery.browser.mobile) {
    $(".left-panel").hide();
  } else {
    $(".left-panel").show();
  }
});
