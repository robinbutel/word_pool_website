var socket = io();
var admin = null;
var name;
var reading = false;
var state = null;
var clients = [];
var words = [];

function is_admin()
{
    return admin;
}
function get_name()
{
    return name;
}
function get_clients()
{
    return clients;
}
function get_words()
{
    return words;
}
function get_state()
{
    return state;
}

function connect(n)
{
    name=n;
    socket.emit('connected',n);
}

function state_change(s)
{
    if(admin!=null)
    {
        state = s;
        if(s == "words") {
            game_state("Choix des mots");
            words_starting();
        } else if(s == "ingame") {
            game_state("Partie en cours");
            we_can_start_this_fun_game();
        } else if(s == "pregame") {
            game_state("Aucune partie en cours");
        }
    }
}

function start_word()
{
    socket.emit('state change', 'words');
}

function new_word(mot)
{
    if(mot!='')
        socket.emit('new word', mot);
}

function add_word(mot)
{
    words.push(mot);
}

function remove_word(i)
{
    socket.emit('remove word', i);
}

function update_words(n)
{
    words=n.slice(0);
}

function next_word()
{
    socket.emit('next word');
}

function skip_word()
{
    socket.emit('skip word');
}

function set_timer(t)
{
    socket.emit('set time', t);
}

function my_turn()
{
  socket.emit('my turn');
}

function start_game()
{
    socket.emit('start game');
}

function reset_game()
{
    socket.emit('reset game');
}

function update_players(n,m)
{
    if(m == 'c')
    {
        clients.push(n);
    }
    else if(m == 'a')
    {
        clients = n.slice(0);
    }
    else
    {
        clients.splice(n,1);
    }
    refresh_clients(clients);
}

function end_turn(t)
{
    stop_timer(t);
}

function round_stop(t)
{
    round_over(t);
    state_change('pregame');
}

$(function(){
    socket.on('admin', () => {
      admin = true;
      you_are_connected(name, true);
      console.log("YOU ARE ADMIN");
    });
    socket.on('client', () => {
      admin = false;
      you_are_connected(name, false);
      console.log("YOU ARE CLIENT");
    });
    socket.on('start timer', (t)=>{ start_timer(t); });
    socket.on('player connected', (n) => { update_players(n,'c'); });
    socket.on('player disconnected', (n) => { update_players(n,'d'); });
    socket.on('update players', (n) => { update_players(n,'a'); });
    socket.on('update words',  (n) => { update_words(n); });
    socket.on('add word', (n) => { add_word(n); });
    socket.on('state change', (n) => { state_change(n); });
    socket.on('in-progress', () => { console.log("partie en cours"); });
    socket.on('turn chosen', () => { not_your_turn(); });
    socket.on('your turn', () => { your_turn(); });
    socket.on('next word', (n) => { show_next_word(n);});
    socket.on('end of round', (w)=> { round_stop(w); } );
    socket.on('turn over', (t)=> { end_turn(t); });
    socket.on('word count', (t)=>{ refresh_words_left(t); });
    socket.on('reset confirmed', ()=> { go_to_div('.admin-setup'); });
});
